Reading these books on your computer  carries a terrible curse -
headaches, eye-strain,  and an itch on the face which constantly moves.
Don't be a fool!  Take the first pdf down to your local, friendly,
print shop instead, or just use the office printer (nobody will notice,
they're all remote-working, leaving you in a crime-friendly ghost-town).


The Goblin Hole
===============

This adventure module has everything you'll need for a one-night game.

1. Rules at the front, 
2. pre-made character sheets at the back (two per player) 
3. a dozen goblins,
4. the motivation of the hangman's noose,
5. and false promises. 

The PCs may think that goblins won't put up much of a fight. They may
imagine themselves striding fearlessly through cavernous hallways,
with an occasional trap. And in the first case, they would be right -
the goblins will flee miles underground while the cavern collapses,
leaving the PCs to navigate a completely natural cavern in the dark.

The cavern has no need of traps when nobody can rush without bashing
their head into a low ceiling, or falling off a dark cliff. Once the
torches run out, they will have to continue in darkness. And once
they find a fire deep underground, it only signals that the goblins
wanted to burn through their oxygen, and their time has become even
more limited.

And when they're exhausted and discussing how much food remains,
while squeezing through a tunnel like a subterranean worm, a single
goblin waits with a dagger and a grin. It stands silently, next to
the narrow tunnel's opening, waiting for a human neck to shuffle out
of the opening, trapped by the human's wide shoulders.


The Rulebooklet
-----------------

Print this single sheet of paper double-sided, and hold your thumb
over the front cover - the one with the word 'BIND' in big letters.
Keep your thumb over the title and fold the longest half of the paper
away from you.

1. Keep folding the longest half away until the page numbers all go up.
2. Pierce the spine, and bind with floss.
3. Cut the leaves open, and you have a wallet-sized, 16-page rulebook.


Stories
=======

Any characters who survived the Goblin Hole can start to build a
backstory by spending their 5 Story Points.  Each  one lets them
introduce some boon from their past  - one PC could say they can
understand the strange Gnollish language as they grew up close to
Gnoll-territory; then when they introduce an ally, it may be a gnoll
they once knew.

And once a character dies, they can roll up a random character, or
take one of their allies as their new character.  The back of the book
has blank character sheets, waiting to be torn out.


Judgement
=========

The Judge's book describes Fenestra through chapters of random
encounter tables. Every facet of life in Fenestra points towards the
fact of wandering monsters - everything from religion to the shape of
farms comes as a response to the things which come out of the
primordial, eternal, forest, which surrounds everything.


Missions in Maitavale
=====================

This Chronicle presents a dozen small tales which slowly weave
together into a chaotic tapestry of political squabbles,
assassinations and competing ideologies.

Every NPC has their statblock printed next to them, whenever they
appear, with notes on mannerisms and goals; and every statblock has
HP-boxes underneath, so you can tick them off with a pen when the
stories turn violent.

When the PCs find a map, you'll find a map handout in the same chapter,
so you can tear it out and give it to them.  Statblocks also exist for
services, so if the PCs hire a guide, you can cut out a small statblock
and let them take care of it.


Core Rules
==========

Every item in the Core Rules is just an interpretation on the same
surface-level rules that the other books use.  It serves as a reference,
and a showcase of how to get a snappy result for any action the players
want their characters to attempt.

