#!/bin/sh

OLD_WORK_DIR="$(pwd)"
missing_file="$OLD_WORK_DIR/missing_links.txt"
base_url="$(grep baseURL hugo.toml  | cut -d= -f2 | tr -d '" ')"
timeout=3
golum=false
process_everything=false
check_remote=false

check_page(){
    curl -sI $base_url/$1 | grep -q ' 404' && {
        echo "The active site does not have $1"
        exit 1
    }
    echo "Found $1, OK."
}

while getopts "ahwvg:c:t" opt; do
  case "${opt}" in
    v)
      echo "using verbose mode"
	  DEBUG=true
      ;;
    c)
		target_dir="$OPTARG"
      ;;
    a)
        process_everything=true
      ;;
    t)
		timeout="$OPTARG"
      ;;
    g)
        golum=true
     ;;
    h)
		echo -e "Options are -v (verbose), -c $dir (change directory) -t N (set timeout, default is 3 seconds), -w (check website) and -a (all files here)."
		exit 0
      ;;
    w)
        check_remote=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

find_markdown_link(){
		grep -Po '\[(.+)\]\(([^ ]+?)( "(.+)")?\)' "$1" | grep -Po '\(\K.+\)' | cut -d ')' -f1
}

sort_into_url_or_file(){
	if [ "$(echo "$1" | grep '://' )" ]; then
		sort_url_types "$1"
	elif [ "$(echo "$1" | grep 'mailto:' )" ]; then
        continue
	else
        if [ "$golum" = "false" ]; then
            check_file_link "$1"
        else
            check_file_link "$1".md
        fi
	fi
}

sort_url_types(){
	if [ "$(echo "$1" | grep -o http)" ]; then
		check_http_link "$1"
	fi
}

check_http_link(){
	curl --connect-timeout "$timeout" -Is "$1" >/dev/null || echo "$1"  >> "$missing_file"
}

check_file_link(){
    file="$(echo "$1" | sed -r "s#/?images/#$OLD_WORK_DIR/assets/images/#")"
	[ -f "$file" ] || echo "$1" >> "$missing_file"
}

check_if_markdown(){
        if [ "$(echo "$1" | rev | cut -d. -f1)" = "dm" ]; then
            if [ "$DEBUG" = "true" ]; then
                echo "$x is markdown"
            fi
        else
            echo "$1 ain't markdown"
            exit 1
        fi
}

####################

[ -z "$target_dir" ] || cd "$target_dir"

if [ "$process_everything" = "true" ]; then
    targets="$(ls *.md)"
else
    targets="$@"
fi

if [ "$check_remote" = "true" ]; then
    #check_page downloads/bind.zip
    for p in about lore intro/workflows; do
        curl -s "$base_url$page" | grep -oE 'src=.*webp' | cut -d= -f2 | cut -d' ' -f1 | \
        while read -r image; do
            check_page "$image"
        done
    done
fi

for x in $targets; do
	check_if_markdown "$x" && (
        find_markdown_link "$x" | while read -r line; do
            [ -n "$DEBUG" ] && echo checking "$line"
            sort_into_url_or_file "$line"
        done &
    ) || (
        echo "$x is not markdown"
    )
done
