## 🚀 Getting Started

Clone the repository with:

```bash
git clone --recurse-submodules https://gitlab.com/bindrpg/bindrpg.gitlab.io
cd bindrpg.gitlab.io
```

### 👉 Project Setup with Docker

Use Docker for the easiest development setup:

```bash
sudo docker build -t bind-hugo -f ./docker/Dockerfile
sudo docker run --rm -it -p 127.0.0.1:1313:1313 -v ./:/src/:Z bind-hugo:latest
```

Or on Arch Linux, with `docker` version 27.2.0:

```bash
docker build -t bind-hugo docker
docker run --rm -it --network host -p 1313:1313 -v ./:/src/:Z bind-hugo:latest
```

Once done, you can visit your website at `http://localhost:1313/`

### Building Locally

The build the various external packages, you'll need these:

- `ffmpeg`, `magick`, `xvfb-run` and `gource` (for the development video).
- `curl` to grab the pdf files, and `zip` to zip them.
- `make`, `git`, and `git-lfs`.

Then run the commands in `docker/entrypoint.sh`.

