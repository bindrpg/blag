DAY_OF_MONTH != date +%d
.PHONY: init
init: rpg_articles/.git wiki/gource/all_images/ fwiki/.git themes/hugoplate/.git layouts/_default/baseof.html layouts/partials/essentials/script.html layouts/partials/essentials/head.html ## initialize submodules

rpg_articles/.git wiki/.git fwiki/.git themes/hugoplate/.git:
	git submodule update --init

layouts/_default/baseof.html: themes/hugoplate/layouts/_default/baseof.html
	mkdir -p $(dir $@)
	@cp $< $@
	@sed -i '/announcement/d' $@
	@sed -i '/adsense/d' $@
	@sed -i '/gtm/d' $@

layouts/partials/essentials/script.html: themes/hugoplate/layouts/partials/essentials/script.html
	mkdir -p $(dir $@)
	@cp $< $@
	@sed -i '/announcement/d' $@
	@sed -i '/adsense/d' $@

layouts/partials/essentials/head.html: themes/hugoplate/layouts/partials/essentials/head.html
	mkdir -p $(dir $@)
	@cp $< $@
	@sed -i '/gtm/d' $@
	@sed -i '/site-verifications/d' $@

ARTICLES != grep -lEm1 -e 'tags:.*BIND' -e 'tags:.*Blorb' rpg_articles/*.md
OUTPUT_ARTICLES = $(patsubst rpg_articles/%,content/english/blog/%,$(ARTICLES))

ARTICLE_IMAGES != ls rpg_articles/images/*
OUTPUT_ARTICLE_IMAGES := $(patsubst rpg_articles/%,assets/%,$(ARTICLE_IMAGES))
ARTICLE_VIDEOS != grep -hPo 'images/\w+.mkv' rpg_articles/*.md
OUTPUT_ARTICLE_VIDEOS = $(patsubst images/%.mkv, assets/images/%.webp, $(ARTICLE_VIDEOS))

$(OUTPUT_ARTICLE_VIDEOS): assets/images/%.webp: rpg_articles/images/%.mkv
	ffmpeg -y -i $< -vf "fps=10,scale=720:-1:flags=lanczos" -vcodec libwebp -lossless 0 -compression_level 6 -q:v 50 -loop 0 -preset picture -an -vsync 0 $@

WIKI_INTRO_ARTICLES != ls wiki/join/*.md

INTRO_ARTICLES := $(patsubst wiki/join/%.md,content/english/intro/%.md,$(WIKI_INTRO_ARTICLES))

WIKI_IMAGES != ls wiki/join/images/*
INTRO_IMAGES := $(patsubst wiki/join/images/%,assets/images/%,$(WIKI_IMAGES))

default += $(OUTPUT_ARTICLES)
default += $(OUTPUT_ARTICLE_IMAGES)
default += $(OUTPUT_ARTICLE_VIDEOS)
default += $(INTRO_ARTICLES)
default += $(INTRO_IMAGES)


.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z._-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

.PHONY: test
test: all ## opens browser and runs site
	command -v xdg-open >/dev/null && xdg-open http://localhost:1313 &
	hugo serve

.PHONY: all
all: init public/ ## compile the absolute lot

.PHONY: check
check: init ## see if you have all the dependencies
	@command -v hugo >/dev/null || { echo "Install hugo" && exit 1 ;}
	@command -v git-lfs >/dev/null || { echo "Install git-lfs" && exit 1 ;}
	@command -v npm >/dev/null || { echo "Install npm" && exit 1 ;}

default += node_modules/
node_modules/:
	npm install
	npm audit fix

default += _vendor/
_vendor/:
	hugo mod clean --all
	hugo mod get -u
	hugo mod vendor
	hugo mod tidy

$(OUTPUT_ARTICLES): $(ARTICLES)

content/english/blog/%: rpg_articles/% | rpg_articles/images/
	@sed 's#(images/#(/images/#g' $< > $@
	@sed -i '/title:/a author: "Malin Freeborn"' $@
	@sed -i 's/.mkv/.webp/g' $@
	@sed -i -r -e 's/"RPG"[, ]?//' -e 's/"BIND"[, ]?//' -e 's/"Memoirs"[, ]?//' $@

$(OUTPUT_ARTICLE_IMAGES): $(ARTICLE_IMAGES)
assets/images/%: rpg_articles/images/%
	@cp $< $@

$(INTRO_ARTICLES): $(WIKI_INTRO_ARTICLES)
$(WIKI_INTRO_ARTICLES): wiki/.git

wiki/join/:
	git submodule update --init wiki
	git submodule foreach git lfs pull
content/english/intro/: $(WIKI_INTRO_ARTICLES) $(WIKI_IMAGES)
	@mkdir -p content/english/intro/

content/english/intro/%: wiki/join/% | content/english/intro/
	@printf "\055--\ntitle: \"$$(basename -s .md $@ | tr '-' ' ' )\"\ntags: [ \"intro\" ]\n---\n" > $@
	@sed -r 's#\.\/(\w+)\)#\1.md)#g' $< >> $@

$(INTRO_IMAGES): $(WIKI_IMAGES)
assets/images/%: wiki/join/images/%
	@cp $< $@

/tmp/bind_rss_$(DAY_OF_MONTH).md: scripts/rss_summary.sh
	./$< > $@

default += content/english/status/_index.md
content/english/status/_index.md: content/english/status/head.md /tmp/bind_rss_$(DAY_OF_MONTH).md | static/video/gource.mp4
	cat $^ > $@

gource_URL = https://gitlab.com/bindrpg/gitlab-profile/-/jobs/artifacts/master/raw/output/gource.mp4?job=build

static/video/gource.mp4:
	mkdir -p $(@D)
	echo '*' > $(@D)/.gitignore
	curl -q --output-dir $(@D) -LO '$(gource_URL)'

default += static/downloads/bind.zip
static/downloads/bind.zip:
	make -j -C $(@D) $(@F)

fwiki/Orrery.png: fwiki/.git
	make -C fwiki/Cosmology/ Orrery.png

assets/images/Orrery.png: fwiki/Orrery.png
	cp $< $@

wiki/gource/all_images/: wiki/.git
	@mkdir -p $@
	make -C wiki/gource/ sorted.txt
	find wiki/gource/ -name "*.jpg" -exec cp -f '{}' $@/ ';'

assets/images/%.jpg: wiki/gource/all_images/%.jpg
	cp $< $@

PROJECT_JPG_COPIES = $(patsubst wiki/gource/all_images/%.jpg,assets/images/%.jpg,$(wildcard wiki/gource/all_images/*))
$(PROJECT_JPG_COPIES): wiki/gource/all_images/

default += $(PROJECT_JPG_COPIES)

public/: $(default)
	npm run build

.PHONY: link-check
link-check: all ## check all links are working
	@./chkmd.sh -c content/english/blog -a
	@./chkmd.sh -c content/english/about -a
	@./chkmd.sh -c content/english/sections -a
	@./chkmd.sh -c content/english/intro -a

clean:
	$(RM) -r $(default)
	$(RM) -r layouts/_default/baseof.html layouts/partials/
	find content/english/blog/ -type f ! -name "_index.md" -delete || true
	find content/english/intro/ -type f ! -name "_index.md" -delete || true

clean-full: clean
	git submodule deinit --all
	make -C static/downloads clean
