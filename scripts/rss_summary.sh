#!/bin/sh

curl -s 'https://gitlab.com/bindrpg.atom' \
| awk 'BEGIN {
		skip=1
	} /<entry>/ {
		skip=0
	} skip == 1 {
		next
	} /<updated>/ {
		sub("^[[:space:]]*<updated>","")
		sub("Z[[:space:]]*</updated>[[:space:]]*$","")
		sub("T"," ")
		tm_stmp="- **" $0 "** -"
	} /<title>/ {
		sub("^[[:space:]]*<title>","")
		sub("</title>[[:space:]]*","")
		gsub("&amp;quot;","\"")
		msg=$0
	} /<link / {
		sub("^[[:space:]]*<link[^>]*href=\"","")
		sub("\".*$","")
		uri="<a href=\"" $0 "\">"
	} /<\/entry>/ && msg != "" {
		print tm_stmp, uri msg "</a>"}' \
| head