---
title: "A community-driven TTRPG"
meta_title: "About"
description: "BIND is an open source TTRPG"
image: "images/wizard_and_cat.jpg"
draft: false
---

BIND (_**B**IND **I**s **N**ot **D**&D_) manifests the philosophy of FOSS (_**F**ree and **O**pen **S**ource **S**oftware_) in a grunge-fantasy TTRPG.

BIND grants four basic freedoms to players and judges alike:

1. The freedom to _use_ the rules for any modules.
2. The freedom to _change_ the rules inside the book, and make your 'house rules' official.
3. The freedom to _redistribute_ your rules and make copies for your players.
4. The freedom to _improve_ the game, and _release_ your improvements (and modified versions in general) to the public, so that the whole community benefits.

## Get in touch

We chat on Matrix, and toot on Mastodon.
As the project evolves more communication channels may be introduced.

### Matrix

Join the BIND Matrix space at [#bindrpg:matrix.org](https://matrix.to/#/#bindrpg:matrix.org) to directly chat with us.
We broadly organize conversations in five different rooms:

| Room         | Description                                                                                                  |
|:----------- |:--------------------------------------------------------------------------------------------------------------|
| General     | Best place to introduce yourself and get in touch with us.                                                    |
| Feedback    | Tell us how we can improve BIND.                                                                              |
| Questions   | Ask questions about BIND mechanics.                                                                           |
| Adventures  | Discuss adventure module ideas/conversions/playtests.                                                         |
| Development | Get your hands dirty on LaTeX, fix typos, help with [open issues](https://gitlab.com/groups/bindrpg/-/issues) |

### Mastodon

You can find 𝕸𝔞𝔩𝔦𝔫 on Mastodon: [@malin@dice.camp](https://dice.camp/@malin)

## BIND in the Wild

BIND has appeared in several places online.

### Podcasts

EN World has a series called 'Not DnD', about non-DnD RPGs.
Jessica's impressive research let the interview quickly cover BIND's most notable features.

- [EN World Interview](https://www.enworld.org/threads/ttrpgs-that-are-not-dnd-for-december.708253)

Get a feel how BIND plays by watching the actual play of the oneshot "Escape from the Horde" on the YouTube channel [*2 Legit 2 Crit*](https://www.youtube.com/@2Legit2Crit):

* [Chatting about BIND](https://www.youtube.com/watch?v=ec2CFWjQFG8)
* [Escape from the Horde Live Play](https://www.youtube.com/watch?v=F_tasBCs9Vg), part 1.
* [Escape from the Horde Live Play](https://www.youtube.com/watch?v=J8uLjJUvZOo), part 2.

Hear Malin join Logan on *The Story Told RPG Podcast* to discuss BIND and the recent OGL controversy:

* [The Story Told Podcast](https://thestorytold.libsyn.com/episode-120-bind-the-ogl-with-malin-freeborn)

### Places to Download

Besides the direct download links BIND is available on [Itch](https://bindrpg.itch.io) and [DrivethruRPG](https://www.drivethrurpg.com/en/publisher/4651/The-Andonome-Coterie).

### Git Repositories

Clone the BIND source in LaTeX on [GitLab](https://gitlab.com/bindrpg) and [GitHub](https://github.com/andonome/core).

### Promotional Resources for Gaming Conventions

BIND provides a small [rules booklet](https://gitlab.com/bindrpg/config/-/jobs/artifacts/master/raw/booklet.pdf?job=build) and a [flyer](/flyer.pdf) ideal for gaming conventions.
