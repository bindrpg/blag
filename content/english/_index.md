---
# Banner
banner:
  title: "BIND RPG"
  content: "BIND is an open-source, grunge-fantasy RPG."
  image: "images/greylands.jpg"
  button:
    enable: true
    label: "Print and Play"
    link: "downloads/bind.zip"

# Features
features:
  - title: "BIND Rules Booklet"
    image: "images/booklet.png"
    content: "The booklet provides the basic rules for the BIND RPG in a small form factor.
    Despite being free, BIND is the most expensive RPG in existence. This rulebook requires time, patience, improvisation, and floss.
    To create your booklet, print it double-sided, then:"
    bulletpoints:
      - "hold the front cover so your right-hand thumb sits over the cover, and"
      - "fold the longest side away from you, and keep your thumb on the cover."
      - "Fold the longest side away from you, and keep your thumb on the cover."
      - "Fold the longest side away from you, and keep your thumb on the cover."
      - "Open the centre, take a needle, and puncture the spine in four places."
      - "Bind with floss."
      - "Slice till done."
    button:
      enable: true
      label: "Print BIND booklet"
      link: "https://gitlab.com/bindrpg/config/-/jobs/artifacts/master/raw/booklet.pdf?job=build"

  - title: "Oneshot: Escape from the Goblin Horde"
    image: "images/waking.jpg"
    content: "Goblins have invaded a gnomish warren, but can't return home, so they are forced to raid nearby human villages for food, such as humans.
    The PCs have been captured, and awaken bound in a cell, with nothing but their clothes, and a depressed prisoner, waiting to die.
    If the PCs can give that prisoner a little hope, he helps them break free."
    button:
      enable: true
      label: "Print Escape from the Goblin Horde"
      link: "https://gitlab.com/bindrpg/oneshot/-/jobs/artifacts/master/raw/Escape_from_the_Goblin_Horde.pdf?job=build"

  - title: "Judgement"
    image: "https://gitlab.com/bindrpg/judgement/-/raw/master/images/Irina/forest.svg?inline=false"
    content: "The *Book of Judgement* describes the world of Fenestra using D6 tables.
    <br><br>
    It contains:"
    bulletpoints:
      - "Random tables to make a 10-point map."
      - "Random tables for every map-point, to add details when needed."
      - "Random tables for Night Guard missions."
      - "One random tables for wandering monsters."
      - "Ecological details on the wandering monsters, from hibernation habits to mating calls."
      - "The guilds which became temples in order to create a divine monopoly on their goods."
      - "The strange gods of death which each temple seeks to thwart, while using as a brand mascot."
    button:
      enable: true
      label: "Print the Book of Judgement"
      link: "https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/Judgement.pdf?job=build"

  - title: "Missions in Maitavale"
    image: "images/wizard_and_cat.jpg"
    content: "*Missions in Maitavale* is a full chronicle, crafted with a dozen smaller Side Quests, all woven together."
    button:
      enable: true
      label: "Print Missions in Maitavale"
      link: "https://gitlab.com/bindrpg/mim/-/jobs/artifacts/master/raw/Missions_in_Maitavale.pdf?job=build"

  - title: "Extended Core Rules"
    image: "images/vitals_shot.jpg"
    content: "The Extended Core Rules have a complete resolution system for any occasion, or activity.
    You don't need any of this to run a game - just think of it as a comforting icon, a symbol that you know what you're talking about, and a fall-back plan for when you suddenly want to know exactly how that Knack works.
    <br><br>
    It contains:"
    bulletpoints:
      - "Examples of play."
      - "Many examples of Skills and Equipment."
      - "Explanations on how the rules work the way they do (CW: small quantities of Maths)."
      - "Myriad combat rules which will make you say <i>'why isn't this standard?'</i>."
      - "All the Knacks."
      - "Too many spells (with a complete alphabetical index)."
      - "Sterile, yet unsettling typography."
    button:
      enable: true
      label: "Print BIND Extended Core Rules"
      link: "https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/Core_Rules.pdf?job=build"

---

### Fenestra is a world, built on a single question.

> How would normal people actually live in a world of wandering monsters?

In the myriad walled baileys, archers guard the fields and the farmers during daylight.

When night comes, people bar their doors, as large predators crawl across their rooves, and feel over the little house for an opening.

And someone has to guard the barrier between civilization and the predators in the dark.  Someone has to walk with the traders from the baileys to the town.

So the town sends their unwanted, unwashed, and understandably bitter brutes, scamps, and political upstarts. 

They send _you_.

## Rules

The rules come in three layers.

- **Layer 1** is a single page of flexible rules, which folds into a little booklet.
- **Layer 2** comes at the start of each module; these are still the basic rules, but with some extra details on situations presented inside the module.
- **Layer 3** is the *Extended Core Rules* - a collection of all rules, with examples, and full details on each Trait.

### Overview

- The entire system uses 2D6 for rolls.
- Only players roll to hit in combat.
- If they miss, the monster hits the player's character.
- Every NPC statblock in the modules is printed on the same page as it's referenced, or the next page.
- Spellcaster NPCs have a short selection of their spells printed next to them.
- Character generation requires 8 rolls of 2D6 to generate the Attributes, concept, and Skills.
- Random character generation ensures players make their first character quickly.
- Players can then hand-craft their allies with Story Points.

### Story Points

The story will follow a group of Night Guards.

Players use Story Points to build their characters' backstory in-game, as they introduce allies and old friends, or explain how they know a language.

Once the character dies, they take up one of the characters they previously introduced, and the group continues to expand and change.
