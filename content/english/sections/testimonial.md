---
enable: false
title: "What Others Are Saying About BIND"
description: "Check out some of our testimonials below to see what others are saying about BIND."

# Testimonials
testimonials:
  - name: "Matija Bijelic"
    designation: "Player & Collaborator"
    avatar: "/images/avatar-sm.png"
    content: "Sturdy enough skeleton for a minimalist, with enough of meat for a simulationist? If you want a situation resolved, you rarely have to look up how to do it. Players can reliably infer which combination of 2D6 + Attribute + Skill is needed to resolve something.
    <br><br>
    Plus, literally light rules-light rule book is a great selling point."

# don't create a separate page
_build:
  render: "never"
---
