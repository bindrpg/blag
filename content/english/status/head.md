---
title: "Development Status"
meta_title: "Status"
description: "A summary of the most recent work done on the server."
image: "images/spiral_staircase.jpg"
draft: false
---

This silent video shows the development history of BIND, from start to finish.

- Each flowery group is a project (most are books).
- Every ball is a file (most are chapters or images).
- People shoot laser-beams of changes, adding or deleting lines.

<video width="640" controls>
  <source src="/video/gource.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

## Recent Activity Summary


