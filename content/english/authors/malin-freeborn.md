---
title: Malin Freeborn
email: malinfreeborn@posteo.net
image: "images/malin.jpg"
description: "Initial BIND author"
social:
  - name: gitlab
    icon: fa-brands fa-gitlab
    link: https://gitlab.com/andonome

  - name: mastodon
    icon: fa-brands fa-mastodon
    link: https://dice.camp/@malin
---

I studied Philosophy in Scotland, but now live in Serbia with my wife and cat.
She's extremely needy, and regularly yells or threatens violence, but most of it's directed at my wife, so I don't mind.

I spend my better nights playing RPGs, and many more writing BIND in LaTeX.
